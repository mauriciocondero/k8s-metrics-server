# Servidor de Métricas do Kubernetes

## Fazer a instalação do metrics-server no Kubernetes

### Aplicar o deploy ```metrics-server-deploy.yaml``` com

```command line
kubectl apply -f /metrics-server-deploy.yaml
```

### Para testar

```command line
kubectl top pods -A
```

```command line
kubectl top nodes
```
